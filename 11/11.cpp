#include "stdafx.h"
#include "11.h"

#include <Windows.h>
#include <math.h>
#include <random>

#define ID_START_BUTTON 101
#define ID_STOP_BUTTON 102
#define ID_LEFT_THREAD 1001
#define ID_RIGHT_THREAD 1002
#define ID_CENTER_THREAD 1003

#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600

#define CNT 16

LPCWSTR g_szClassName = L"threads";
HINSTANCE hInst;
HWND hWnd, Start_Button, Stop_Button;
HANDLE threads[CNT];
bool threads_exist = FALSE;
bool stop_flag = TRUE;
POINT source_point;

std::default_random_engine gen;
std::uniform_real_distribution<double> distr_dir(-3, 803);
std::uniform_real_distribution<double> distr_color(0, 255);
std::uniform_real_distribution<double> distr_speed(0, 5);

struct Ray
{
	HPEN hPen;
	POINT direction;
	POINT coords;
	DWORD speed;
};

DWORD WINAPI DrawRay(LPVOID lpParam);

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_CREATE:
	{
		Start_Button = CreateWindow(L"Button", L"Start", WS_CHILD | WS_VISIBLE | WS_BORDER, 30, 500, 100, 30, hwnd,
			(HMENU)ID_START_BUTTON, hInst, NULL);
		Stop_Button = CreateWindow(L"Button", L"Stop", WS_CHILD | WS_VISIBLE | WS_BORDER, 160, 500, 100, 30, hwnd,
			(HMENU)ID_STOP_BUTTON, hInst, NULL);
		break;
	}
	case WM_COMMAND:
	{
		switch (LOWORD(wParam))
		{
		case ID_START_BUTTON:
			if (stop_flag) {
				if (threads_exist)
				{
					for (size_t i = 0; i < CNT; i++)
					{
						ResumeThread(threads[i]);
					}
				}
				else
				{
					source_point.x = 450;
					source_point.y = 250;

					Ray rays[CNT];

					for (int i = 0; i < CNT; i++)
					{
						auto color = RGB(distr_color(gen), distr_color(gen), distr_color(gen));
						rays[i].hPen = CreatePen(PS_SOLID, 5, color);

						rays[i].direction.x = distr_dir(gen);
						rays[i].direction.y = distr_dir(gen);
						rays[i].coords.x = 450;
						rays[i].coords.y = 250;

						rays[i].speed = long(distr_speed(gen));
					}

					for (int i = 0; i < CNT; i++)
					{
						threads[i] = CreateThread(NULL, NULL, DrawRay, &rays[i], NULL, NULL);
					}
					threads_exist = TRUE;
				}
				stop_flag = FALSE;
			}
			break;

		case ID_STOP_BUTTON:
			if (!stop_flag)
			{
				for (size_t i = 0; i < CNT; i++)
				{
					SuspendThread(threads[i]);
				}
				stop_flag = TRUE;
			}
			break;
		}
		break;
	}
	case WM_GETMINMAXINFO:
	{
		MINMAXINFO* mmi = (MINMAXINFO*)lParam;
		mmi->ptMinTrackSize.x = WINDOW_WIDTH;
		mmi->ptMinTrackSize.y = WINDOW_HEIGHT;
		mmi->ptMaxTrackSize.x = WINDOW_WIDTH;
		mmi->ptMaxTrackSize.y = WINDOW_HEIGHT;
		break;
	}
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hwnd, msg, wParam, lParam);
	}
	return 0;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpCmdLine, int nCmdShow)
{
	WNDCLASSEX wc;
	HWND hwnd;
	MSG Msg;

	hInst = hInstance;
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = 0;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = g_szClassName;
	wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	if (!RegisterClassEx(&wc))
	{
		MessageBox(NULL, L"Window Registration Failed!", L"Error!",
			MB_ICONEXCLAMATION | MB_OK);
		return 0;
	}

	hwnd = CreateWindowEx(
		WS_EX_CLIENTEDGE,
		g_szClassName,
		L"Threads",
		WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX,
		CW_USEDEFAULT, CW_USEDEFAULT, WINDOW_WIDTH, WINDOW_HEIGHT,
		NULL, NULL, hInstance, NULL);

	if (hwnd == NULL)
	{
		MessageBox(NULL, L"Window Creation Failed!", L"Error!",
			MB_ICONEXCLAMATION | MB_OK);
		return 0;
	}

	hWnd = hwnd;

	ShowWindow(hwnd, nCmdShow);
	UpdateWindow(hwnd);

	while (GetMessage(&Msg, NULL, 0, 0) > 0)
	{
		TranslateMessage(&Msg);
		DispatchMessage(&Msg);
	}
	return Msg.wParam;
}

DWORD WINAPI DrawRay(LPVOID lpParam)
{
	HPEN hPenWhite = CreatePen(PS_SOLID, 5, RGB(255, 255, 255));
	HDC hdc = GetDC(hWnd);
	
	Ray ray = *((Ray*)lpParam);
	
	Sleep(ray.speed);

	while (TRUE) {
		if (ray.coords.y > 470 || ray.coords.y < 0 || ray.coords.x > 800 || ray.coords.x < 0) {

			SelectObject(hdc, hPenWhite);
			MoveToEx(hdc, ray.coords.x, ray.coords.y, NULL);
			LineTo(hdc, source_point.x, source_point.y);

			DeleteObject(ray.hPen);
			ray.hPen = CreatePen(PS_SOLID, 5, RGB(distr_color(gen), distr_color(gen), distr_color(gen)));

			ray.direction.x = distr_dir(gen);
			ray.direction.y = distr_dir(gen);

			ray.coords.y = 250;
			ray.coords.x = 450;

			ray.speed = long(distr_speed(gen));
		}
		else {
			SelectObject(hdc, hPenWhite);
			MoveToEx(hdc, ray.coords.x, ray.coords.y, NULL);
			LineTo(hdc, source_point.x, source_point.y);

			ray.coords.x = source_point.x + (ray.direction.x - source_point.x) *
				(sqrt(pow(ray.coords.x - source_point.x, 2) + pow(ray.coords.y - source_point.y, 2)) + 3.0) /
				sqrt(pow(ray.direction.x - source_point.x, 2) + pow(ray.direction.y - source_point.y, 2));

			ray.coords.y = source_point.y + (ray.direction.y - source_point.y) *
				(sqrt(pow(ray.coords.x - source_point.x, 2) + pow(ray.coords.y - source_point.y, 2)) + 3.0) /
				sqrt(pow(ray.direction.x - source_point.x, 2) + pow(ray.direction.y - source_point.y, 2));
			
			SelectObject(hdc, ray.hPen);
			MoveToEx(hdc, source_point.x, source_point.y, NULL);
			LineTo(hdc, ray.coords.x, ray.coords.y);
		}
		Sleep(ray.speed);
	}
	return 0;
}